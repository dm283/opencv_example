# СКРИПТ#2 === Алгоритм проверки аутентичности содержимого контейнераизображения ===

import sys
import os
#sys.path.append('c:\\Users\\omali\\Documents\\01_VSU\\02_Информационная_безопасность\\Лабораторная_1\\lab1\\env\\Lib\\site-packages')
new_path = os.getcwd()+'\\env\\Lib\\site-packages'
sys.path.append(new_path)

import numpy as np
import cv2

def viewImage(image, name_of_window):
    """вывести изображение"""
    cv2.namedWindow(name_of_window, cv2.WINDOW_NORMAL)
    cv2.imshow(name_of_window, image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

def split_to_blocks(img):
    """Разбиение оригинального изображения размером 640x640 пикселей на непересекающиеся блоки 
    фиксированного размера 64x64 пикселей"""
    ranges_points = list(range(0, img.shape[0], 64))
    ranges_points.append(640)
    #print(ranges_points)

    squares = list()  # список матриц-квадратов (64х64) исходного изображения
    for y in range( len(ranges_points)-1 ):
        for x in range( len(ranges_points)-1 ):
            y1, y2 = ranges_points[y:y+2]
            x1, x2 = ranges_points[x:x+2]
            squares.append( img[y1:y2, x1:x2] )

    #print(squares[45].shape, len(squares))
    return squares, ranges_points

def assembly_image_from_blocks(squares, ranges_points):
    """Сборка матрицы изображения из блоков"""
    img_assembled = np.ndarray(shape=img.shape, dtype=np.uint8)  # создаем каркас матрицы изображения (размеры равны исходному изображению)

    sqr_counter = 0
    for y in range( len(ranges_points)-1 ):
        for x in range( len(ranges_points)-1 ):
            y1, y2 = ranges_points[y:y+2]
            x1, x2 = ranges_points[x:x+2]

            img_assembled[y1:y2, x1:x2] = squares[sqr_counter]
            sqr_counter += 1

    #print(img_assembled.size, img_assembled.shape)
    #print(img.size, img.shape)
    return img_assembled

def calc_average_hash(squares):
    """Вычисление перцептивного хеш-кода по среднему aHash (Average hash) и формирование массива векторов с хешем"""
    vectors = list()
    for sqr in squares:
        v = cv2.resize(sqr, (8, 8))              #1) Уменьшение размера каждого изображения до 8x8 пикселей (длина хеш 64 бита)
        v = cv2.cvtColor(v, cv2.COLOR_BGR2GRAY)  #2) Приведение к представлению в градациях серого
        v = cv2.threshold(v, 127, 1, 0)[1]       #3) Бинарная сегментация элементов вектора
        v = np.resize( v, (1, 64) )[0]           #4) Построчная развертка пикселей матрицы 8x8 в вектор
        vectors.append(v)                        #5) Формируем массив векторов
    #print(vectors[0].shape, len(vectors))
    
    return vectors

def embed(x, m):
    """ Алгоритм QIM для встраивания информации в массив данных
        x - вектор данных, куда нужно встроить информацию; m - бинарный вектор с информацией для встраивания
        returns: a quantized vector y """
    x = x.astype(float)
    d = 2 #  delta = шаг квантования - как выбирается ??? при его больших значениях картинка с искажениями!
    #y = np.round(x/d) * d + (-1)**(m+1) * d/4.
    y = np.round(x/d) * d + (d/2)*m
    y = y.astype(np.uint8)
    return y

def detect(z):
    """
    z is the received vector, potentially modified
    returns: a detected vector z_detected and a detected message m_detected
    """
    shape = z.shape
    z = z.flatten()

    m_detected = np.zeros_like(z, dtype=float)
    z_detected = np.zeros_like(z, dtype=float)

    z0 = embed(z, 0)
    z1 = embed(z, 1)

    d0 = np.abs(z - z0)
    d1 = np.abs(z - z1)

    gen = zip(range(len(z_detected)), d0, d1)
    for i, dd0, dd1 in gen:
        if dd0 < dd1:
            m_detected[i] = 0
            z_detected[i] = z0[i]
        else:
            m_detected[i] = 1
            z_detected[i] = z1[i]

    z_detected = z_detected.reshape(shape)
    m_detected = m_detected.reshape(shape)
    return z_detected, m_detected.astype(int)

def detect_hash(squares):
    """ Извлечение из каждого блока стеганографически скрытых зашифрованных перцептивных хеш-кодов """
    hsh_detected = list()
    for i in range( len(squares) ):
        z_detected, msg = detect( squares[i][0].T[0] )
        hsh_detected.append( msg )
    #print(hsh_detected[0].shape, len(hsh_detected))
    
    return hsh_detected

def count_disparity(vectors, hsh_detected):
    """ Подсчёт количества несоответствий в каждом блоке """
    disparity = dict()
    for i in range( len(vectors) ):
        for h in range ( vectors[i].shape[0] ):
            if vectors[i][h] != hsh_detected[i][h]:  # в пром варианте здесь проверка на !=
                if i in disparity.keys():
                    disparity[i] += 1
                else:
                    disparity[i] = 1
    return disparity

def mark_corrupted_squares(disparity, squares):
    """ Выделение измененных блоков изображения """
    for d in disparity.keys():
        cv2.rectangle(squares[d], (0, 0), (64, 64), (0, 0, 255), 5)
    return squares


# Шаг 0. Загрузка изображения для проверки
f_name = sys.argv[1]
print("\nЗагрузка изображения ==", end=' ')
img = cv2.imread(f_name)
#img = cv2.imread("pic_with_hash.png")
print("[DONE]")
print("Размерность изображение ==", img.shape)
viewImage(img, "img_for_check")

# Шаг 1. Разбиение маркированного изображения размером 640x640 пикселей на непересекающиеся блоки 
#        фиксированного размера пикселей 64x64
print("Разбиение маркированного изображения на непересекающиеся блоки фиксированного размера ==", end=' ')
squares, ranges_points = split_to_blocks(img)
print("[DONE]")

# Шаг 2. Выделение низкочастотных информативно-значимых составляющих для каждого блока по алгоритму, использовавшемуся при
#        формировании маркированного контейнера
print("Выделение низкочастотных информативно-значимых составляющих для каждого блока ==", end=' ')
vectors = calc_average_hash(squares)
print("[DONE]")

# Шаг 3. Извлечение из каждого блока стеганографически скрытых зашифрованных перцептивных хеш-кодов
print("Извлечение из каждого блока стеганографически скрытых зашифрованных перцептивных хеш-кодов ==", end=' ')
hsh_detected = detect_hash(squares)
print("[DONE]")

# Шаг 4. Сравнение вычисленного (vectors) и извлеченного (hsh_detected) хешзначений 
#        с использованием заданной функции расстояния
print("Сравнение вычисленного и извлеченного хешзначений ==", end=' ')
disparity = count_disparity(vectors, hsh_detected)
print("[DONE]")

# Выделение измененных блоков изображения
if len(disparity.keys()):
    print("\nПроведена проверка:  исходное изображение было изменено.")
    print("Измененные блоки изображения выделены красными квадратами." + "033[00m")
    squares = mark_corrupted_squares(disparity, squares)
    # Пересобираем изображение из проверенных на изменение блоков
    img_checked = assembly_image_from_blocks(squares, ranges_points)
    viewImage(img_checked, "img_checked")
else:
    print("\nПроведена проверка:  исходное изображение не было изменено.")
