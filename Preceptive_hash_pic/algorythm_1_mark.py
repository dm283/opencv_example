# СКРИПТ#1 === Алгоритм создания защищенного - маркированного контейнера ===

import sys
import os
#sys.path.append('c:\\Users\\omali\\Documents\\01_VSU\\02_Информационная_безопасность\\Лабораторная_1\\lab1\\env\\Lib\\site-packages')
new_path = os.getcwd()+'\\env\\Lib\\site-packages'
sys.path.append(new_path)

import numpy as np
import cv2

def viewImage(image, name_of_window):
    """вывести изображение"""
    cv2.namedWindow(name_of_window, cv2.WINDOW_NORMAL)
    cv2.imshow(name_of_window, image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

def split_to_blocks(img):
    """Разбиение оригинального изображения размером 640x640 пикселей на непересекающиеся блоки 
    фиксированного размера 64x64 пикселей"""
    ranges_points = list(range(0, img.shape[0], 64))
    ranges_points.append(640)
    #print(ranges_points)

    squares = list()  # список матриц-квадратов (64х64) исходного изображения
    for y in range( len(ranges_points)-1 ):
        for x in range( len(ranges_points)-1 ):
            y1, y2 = ranges_points[y:y+2]
            x1, x2 = ranges_points[x:x+2]
            squares.append( img[y1:y2, x1:x2] )

    #print(squares[45].shape, len(squares))
    return squares, ranges_points

def assembly_image_from_blocks(squares, ranges_points):
    """Сборка матрицы изображения из блоков"""
    img_assembled = np.ndarray(shape=img.shape, dtype=np.uint8)  # создаем каркас матрицы изображения (размеры равны исходному изображению)

    sqr_counter = 0
    for y in range( len(ranges_points)-1 ):
        for x in range( len(ranges_points)-1 ):
            y1, y2 = ranges_points[y:y+2]
            x1, x2 = ranges_points[x:x+2]

            img_assembled[y1:y2, x1:x2] = squares[sqr_counter]
            sqr_counter += 1

    #print(img_assembled.size, img_assembled.shape)
    #print(img.size, img.shape)
    return img_assembled

def calc_average_hash(squares):
    """Вычисление перцептивного хеш-кода по среднему aHash (Average hash) и формирование массива векторов с хешем"""
    vectors = list()
    for sqr in squares:
        v = cv2.resize(sqr, (8, 8))              #1) Уменьшение размера каждого изображения до 8x8 пикселей (длина хеш 64 бита)
        v = cv2.cvtColor(v, cv2.COLOR_BGR2GRAY)  #2) Приведение к представлению в градациях серого
        v = cv2.threshold(v, 127, 1, 0)[1]       #3) Бинарная сегментация элементов вектора
        v = np.resize( v, (1, 64) )[0]           #4) Построчная развертка пикселей матрицы 8x8 в вектор
        vectors.append(v)                        #5) Формируем массив векторов
    #print(vectors[0].shape, len(vectors))
    
    return vectors

def embed(x, m):
    """ Алгоритм QIM для встраивания информации в массив данных
        x - вектор данных, куда нужно встроить информацию; m - бинарный вектор с информацией для встраивания
        returns: a quantized vector y """
    x = x.astype(float)
    d = 2 #  delta = шаг квантования - как выбирается ??? при его больших значениях картинка с искажениями!
    #y = np.round(x/d) * d + (-1)**(m+1) * d/4.
    y = np.round(x/d) * d + (d/2)*m
    y = y.astype(np.uint8)
    return y

def embed_message_to_squares(squares, vectors):
    """Заливаем сообщение в каждый блок squares 
    (в первый столбец первой строки squares[i] (т.к. каждый блок трехмерный, цветной) заливается vector[i])"""
    squares_moded = list()
    for i in range( len(squares) ):
        string_for_mode = squares[i][0].T[0].copy()  # выбираем первую строку блока
        string_moded = embed(string_for_mode, vectors[i])
        sqr_moded = squares[i].copy()
        
        for s in range( vectors[0].shape[0] ):
            sqr_moded[0][s][0] = string_moded[s]
        
        squares_moded.append(sqr_moded)
    return squares_moded


# Шаг 0. Загрузка изображения
f_name = sys.argv[1]
print("\nЗагрузка изображения ==", end=' ')
#img = cv2.imread("pic_source.png")
img = cv2.imread(f_name)
print("[DONE]")
print("Размерность изображение ==", img.shape)
viewImage(img, "pic_source")

# Шаг 1. Разбиение оригинального изображения размером 640x640 пикселей на непересекающиеся блоки 
#        фиксированного размера 64x64 пикселей
print("Разбиение оригинального изображения на непересекающиеся блоки фиксированного размера == ", end=' ')
squares, ranges_points = split_to_blocks(img)
print("[DONE]")

# Шаг 2. Вычисление перцептивного хеш-кода по среднему aHash (Average hash)
print("Вычисление перцептивного хеш-кода по среднему aHash ==", end=' ')
vectors = calc_average_hash(squares)
print("[DONE]")

# Шаг 4. Стеганографическое скрытие хеш-кода в соответствующий ему блок изображения
# заливаем сообщение в каждый блок squares (в первую строку squares[i] заливается hsh_vector[i])
print("Стеганографическое скрытие хеш-кода в соответствующий ему блок изображения")
print("    =1= запись сообщения в каждый блок (в первую строку) ==", end=' ')
squares_moded = embed_message_to_squares(squares, vectors)
print("[DONE]")

# Шаг 5. Из блоков формируется итоговое маркированное изображение
print("    =2= формирование из блоков итогового маркированного изображения ==", end=' ')
img_with_hash = assembly_image_from_blocks(squares_moded, ranges_points)
print("[DONE]")
viewImage(img_with_hash, "image_with_hash")

# сохраняем изображение со скрытым хешем
f_name_w = f_name.partition('.')[0] + '_marked.png'
print(f"Сохранение изображения со скрытым хешем в файл {f_name_w} ==", end=' ')
#cv2.imwrite("pic_with_hash.png", img_with_hash)   # сохраняем изображение
cv2.imwrite(f_name_w, img_with_hash)   # сохраняем изображение
print("[DONE]")
